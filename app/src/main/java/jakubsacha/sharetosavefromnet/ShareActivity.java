package jakubsacha.sharetosavefromnet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class ShareActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        WebView webView = (WebView)findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.addJavascriptInterface(new SaveFromNetWebInterface(this), "Android");


        // Get intent, action and MIME type
        final Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && "text/plain".equals(type)) {
            webView.loadUrl("http://en.savefrom.net/");
            webView.setWebViewClient(new WebViewClient() {

                public void onPageFinished(WebView view, String url) {
                    view.evaluateJavascript("$('.install-now, .wrapper').remove();", null);
                    view.evaluateJavascript("$('#sf_url').val('"+intent.getStringExtra(Intent.EXTRA_TEXT)+"');$('#sf_url').focus(); $('#sf_submit').click();", null);
                    view.evaluateJavascript("$('body').on('click', '.def-btn-box .link-download', function(e){Android.download($(e.target).attr('href'));e.stopPropagation(); e.preventDefault();return false;})", null);
                }
            });
        } else {
            Intent hp = new Intent(ShareActivity.this, MainScreen.class);
        }


    }
}
