package jakubsacha.sharetosavefromnet;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.webkit.JavascriptInterface;

class SaveFromNetWebInterface {
    private Context context;

    public SaveFromNetWebInterface(Context context) {
        this.context = context;
    }

    @JavascriptInterface
    public void download(String url){
        DownloadManager downloadmanager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
        downloadmanager.enqueue(request);
    }
}
